import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class FileWriter2 {
    public static void writeStringToFile(String outputFile, String data) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new java.io.FileWriter(outputFile));
            writer.write(data);
        }
        catch (IOException e) {
            System.out.println("Writing error <<<");
        }
        finally {
            try {
                if (writer != null)
                    writer.close();
            }
            catch (IOException e) {
                System.out.println("Flie Closing error <<<");
            }
        }
    }
    public static void appendNewLineToFile(String outputFile, String line){
        try {
            Writer writer = new BufferedWriter(new FileWriter(outputFile, true));
            writer.write(line + "\n");
            writer.close();
        } catch (Exception e) {
            System.out.println("Writing error");
        }
    }
}