import java.io.*;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class DataPreProcessing {
    static public String cincinnatiDir = "C://Users//aries//Desktop//cincinnati//";
    static public String cincinnatiDir_processed = "C://Users//aries//Desktop//cincinnati//processed//";


    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.SECONDS);
    }

//    DataPreProcessing.readBusData("./data/mta_1706.csv","./data/mta_1706_new.csv");
    static public void readBusData(String inputFile, String outputFile) {
        // RecordedAtTime,
        // DirectionRef,PublishedLineName,OriginName,OriginLat,OriginLong, DestinationName,
        // DestinationLat,DestinationLong,VehicleRef,VehicleLocation.Latitude,VehicleLocation.Longitude,
        // NextStopPointName,ArrivalProximityText,DistanceFromStop,ExpectedArrivalTime,ScheduledArrivalTime

        String str = FileReader2.readStringFromFile(inputFile);
        System.out.println(str.length());
        String[] lines = str.split("\n");
        System.out.println(lines.length);
        Set<Integer> sellected = new HashSet<>(Arrays.asList(0, 7, 8, 10, 11, 15));
        String[] new_atts = {"id", "pickup_datetime", "dropoff_datetime",
                                    "pickup_latitude","pickup_longitude",
                                    "dropoff_latitude","dropoff_longitude",
                                        "trip_duration"};

        StringBuilder processedData = new StringBuilder();

        for (String ss : new_atts)
            processedData.append(processedData.length() > 0 ? "," : "").append(ss);
        processedData.append("\n");

        int cntID = 0;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        for (String line : lines) {
            String[] atts = line.split(",");

            StringBuilder sb = new StringBuilder(Integer.toString(++cntID));
            sb.append(",");

            long duration = 0;
            // check
            for (int i : sellected)
                if (atts[i] == null  || atts[i].startsWith("NA") || atts[i].length() < 1) continue;

            try {
                String t1 = atts[0].replace("2017", "2016");
                String t2 = atts[15].replace("2017", "2016");
                Date pickuptime = formatter.parse(t1);
                Date dropofftime = formatter.parse(t2);
                duration = getDateDiff(pickuptime,dropofftime,TimeUnit.MINUTES);
                if (duration <= 100) continue;

                sb.append(t1).append(",");
                sb.append(t2).append(",");
                sb.append(atts[10]).append(",").append(atts[11]).append(",");
                sb.append(atts[7]).append(",").append(atts[8]).append(",");
                sb.append(duration).append("\n");

            } catch (Exception e) {
                continue;
            }


            processedData.append(sb.toString());

            if (cntID == 50000) {
                FileWriter2.writeStringToFile("./data/mta_1706_small.csv", processedData.toString());
            }
        }
        FileWriter2.writeStringToFile(outputFile, processedData.toString());
        System.out.println("new data size = " + cntID);
    }


    static public void process_history_vehicle_GPS_streaming(String fileName, String outputFile)  {
        int cnt = 0, cnt2 = 0;
        List<Integer> sellected = Arrays.asList(0, 3, 5, 7, 8, 12, 15);

        Set<String> vehicles = new HashSet<>();

        FileWriter2.appendNewLineToFile(outputFile,"ASSET,HEADING,LATITUDE,LONGITUDE,ODOMETER,SPEED,TIME");

        try {
            FileReader file = new FileReader(fileName);
            try (BufferedReader br = new BufferedReader(file)) {
                String line;
                while ((line = br.readLine()) != null) {
                    ++cnt;
                    if (cnt > 1000000) break;

                    String[] att = line.split(",");
                    if (att[0].equals("ASSET")) continue;

                    StringBuilder tmp = new StringBuilder();
                    boolean ok = true;

                    vehicles.add(att[0]);

                    for (int id : sellected)
                        if (att[id] != null && att[id].length() > 0) {
                            try {
                                if (id == 12 && Double.parseDouble(att[id]) < 0)
                                    ok = false;
                            }  catch (Exception e) {
                                ok = false;
                            }
                            if (id == 15)
                                tmp.append(changeFormatDate(att[id]));
                            else
                                tmp.append(att[id]);
                            if (id != 15) tmp.append(",");
                        } else ok = false;
                    if (ok) {
                        FileWriter2.appendNewLineToFile(outputFile, tmp.toString());
                        ++cnt2;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            System.out.println("Exception error");
        }
        System.out.println("#Line = " + cnt);
        System.out.println("Number of vehicles = " + vehicles.size());
        System.out.println("Size of new data = " + cnt2);
    }
    static public void process_history_vehicle_GPS(String file) {
//        String file = "Historic_Vehicle_GPS_Data__Department_of_Public_Services__2016_.csv";
        String str = FileReader2.readStringFromFile(cincinnatiDir + file);
        String[] lines = str.split("\n");
        System.out.println(lines.length);

        StringBuilder sb = new StringBuilder();

//        [ASSET],ASSETNHOOD,[DISTANCE_TRAVELED],[HEADING],[ID_HAM_PVMNT_PLYGN],
//        [LATITUDE],LOADTS,[LONGITUDE],[ODOMETER],PPOLYLABEL,REASONS,REASONS_TEXT,
//        [SPEED],STREETFROM,STREETTO,[TIME]

        List<Integer> sellected = Arrays.asList(0, 3, 5, 7, 8, 12, 15);

        Set<String> vehicles = new HashSet<>();
        sb.append("ASSET,HEADING,LATITUDE,LONGITUDE,ODOMETER,SPEED,TIME\n");

        int cnt = 0;
        for (int i = 1; i < lines.length; ++i) {
            String line = lines[i];
            String[] att = line.split(",");
            StringBuilder tmp = new StringBuilder();
            boolean ok = true;

            vehicles.add(att[0]);

            for (int id : sellected)
                if (att[id] != null && att[id].length() > 0) {
                    try {
                        if (id == 12 && Double.parseDouble(att[id]) < 10)
                            ok = false;
                    }  catch (Exception e) {
                        ok = false;
                    }
                    if (id == 15 && !att[id].equals("TIME"))
                        tmp.append(changeFormatDate(att[id]));
                    else
                        tmp.append(att[id]);
                    if (id != 15) tmp.append(",");
                } else ok = false;
            if (ok) {
                sb.append(tmp.toString()).append("\n");
                ++cnt;
            }
        }
        System.out.println("Number of vehicles = " + vehicles.size());
        System.out.println("Size of data = " + cnt);
        FileWriter2.writeStringToFile(cincinnatiDir_processed + file, sb.toString());
    }

    static public void processCrashReport_Cincinnati(String fileName, String output) {
        String data = FileReader2.readStringFromFile(fileName);
        String[] lines = data.split("\n");
        System.out.println("#Lines = " + lines.length);
        List<Integer> selected = Arrays.asList(1,2,10,15,18,19,20,23);
//        ADDRESS_X,
//        LATITUDE_X,LONGITUDE_X,
//        AGE,COMMUNITY_COUNCIL_NEIGHBORHOOD,CPD_NEIGHBORHOOD,
//        CRASHDATE,CRASHLOCATION,CRASHSEVERITY,CRASHSEVERITYID,
//        DATECRASHREPORTED,
//        DAYOFWEEK,GENDER,INJURIES,INSTANCEID,
//        LIGHTCONDITIONSPRIMARY,
//        LOCALREPORTNO,MANNEROFCRASH,
//        ROADCONDITIONSPRIMARY,ROADCONTOUR,ROADSURFACE,
//        SNA_NEIGHBORHOOD,TYPEOFPERSON,
//        WEATHER,
//        ZIP,UNITTYPE

        int cnt = 0;
        StringBuilder sb = new StringBuilder();
        sb.append("ID,LATITUDE,LONGITUDE,DATE,LIGHTCONDITIONSPRIMARY,ROADCONDITIONSPRIMARY,ROADCONTOUR,ROADSURFACE,WEATHER\n");
        for (int i = 1; i < lines.length; ++i) {
            StringBuilder line = new StringBuilder(lines[i]);
//            System.out.println(i);
            StringBuilder tmp = new StringBuilder();
            boolean ok = true;
            if (line.toString().contains("\"")) {
//                ok = false;
                line = cleanDoubleQuote(line.toString());
            }
            String[] atts = line.toString().split(",");
//            System.out.println("-"+atts.length);
            if (!atts[10].contains("2018"))
                ok = false;
            if (ok)
                for (int id : selected)
                    if (atts[id] == null || atts[id].length() < 1) {
                        ok = false;
                        break;
                    }
            if (ok) {
                tmp.append(++cnt);
                for (int id : selected) {
                    if (id ==10) {
                        tmp.append(",").append(changeFormateDate2(atts[id]));
                    } else
                    tmp.append(",").append(atts[id]);
                }
                tmp.append("\n");
                sb.append(tmp.toString());
            }
        }
        System.out.println("Size of new data = " + cnt);
        FileWriter2.writeStringToFile(output, sb.toString());
    }

    static public StringBuilder cleanDoubleQuote(String str) {
        int last = -1;
        StringBuilder sb = new StringBuilder(str);
        for (int i = 0; i < str.length(); ++i) {
            if (str.charAt(i) == '\"') {
                if (last == -1)
                    last = i;
                else {
                    for (int j = last + 1; j < i; ++j)
                        if (str.charAt(j) == ',')
                            sb.setCharAt(j, ' ');
                    sb.setCharAt(last, ' ');
                    sb.setCharAt(i, ' ');
                    last = -1;
                }
            }
        }
        return sb;
    }
    static public Date convertFromTimeStampToDate(String timeStamp) {
        //Timestamp ts = Timestamp.valueOf(timeStamp);
        long t = Long.parseLong(timeStamp);
        Date date = new Date(t);
        System.out.println(date);
        return date;
    }

    static public String dateToString(Date date) {
        String pattern = "yyyy-MM-dd HH:mm:ss";
        DateFormat df = new SimpleDateFormat(pattern);
        String toString = df.format(date);
        System.out.println(toString);
        return toString;
    }
    static public void process_blockage_delays(String fileName, String output){
        String data = FileReader2.readStringFromFile(fileName);
        String[] lines = data.split("\n");
        System.out.println("#Lines = " + lines.length);
        // col 4 is time
        List<Integer> selected = Arrays.asList(1,3,12,13);
        StringBuilder sb = new StringBuilder();
        sb.append("ID,TIME,DURATION,LATITUDE,LONGITUDE\n");
        int cnt = 0;
        for (int i = 1; i < lines.length; ++i) {
            String line = lines[i];
            if (line.contains("\"")) continue;

            StringBuilder tmp = new StringBuilder();
            String[] atts = line.split(",");
            boolean ok = true;
            for (int id : selected)
                if (atts[id] == null || atts[id].length() < 1) ok = false;
            if (!atts[1].contains("2018"))
                ok = false;
            if (ok) {
                float duration = Float.parseFloat(atts[3]);
                tmp.append(++cnt).append(",").append(changeFormateDate2(atts[1])).append(",").append((int)(duration * 60)).append(",").append(atts[12]).append(",").append(atts[13]).append("\n");
                sb.append(tmp.toString());
            }
        }
        System.out.println("Size of new data = " + cnt);
        FileWriter2.writeStringToFile(output, sb.toString());
    }

    static public void preprocess_cincinnati(){
        //process_history_vehicle_GPS("Historic_Vehicle_GPS_Data__Department_of_Public_Services__2016_.csv");
        process_history_vehicle_GPS_streaming(cincinnatiDir + "Historic_Vehicle_GPS_Data__Department_of_Public_Services__2018_.csv",
                cincinnatiDir_processed + "Historic_Vehicle_GPS_Data__Department_of_Public_Services__2018_.csv");
//        processCrashReport_Cincinnati(cincinnatiDir + "Traffic_Crash_Reports__CPD_2013_2020.csv",
//                cincinnatiDir_processed + "Traffic_Crash_Reports__CPD_2018.csv");
//        process_blockage_delays(cincinnatiDir + "Streetcar_Blockages_and_Delays_2018_2019.csv",
//                cincinnatiDir_processed + "Streetcar_Blockages_and_Delays_2018.csv");
    }

    // 20161108024131
    static public String changeFormatDate(String date) {
        StringBuilder sb = new StringBuilder();
        sb.append(date, 0, 4).append("-").append(date, 4, 6).append("-").append(date, 6, 8).append(" ");
        sb.append(date, 8,10).append(":").append(date, 10, 12).append(":").append(date, 12, 14);
        return sb.toString();
    }

    static public String changeFormateDate2(String date) {
        StringBuilder sb = new StringBuilder();
        //02/01/2015 02:47:17 AM, OR : 07/28/2018 12:00:00 AM +0000
        //"yyyy-MM-dd HH:mm:ss"
        sb.append(date, 6, 10).append("-").append(date, 0, 2).append("-").append(date, 3, 5).append(" ");
        String hour = date.substring(11, 13);
        if (date.charAt(20) == 'A'){
            if ("12".equals(hour)) hour = "00";
        } else {
            if (!"12".equals(hour)) {
                int hourInt = Integer.parseInt(hour) + 12;
                hour = Integer.toString(hourInt);
            }
        }

        String minute = date.substring(14, 16);
        String second = date.substring(17, 19);
        sb.append(hour).append(":").append(minute).append(":").append(second);

        return sb.toString();
    }
    public static void main(String[] args) {
//        DataPreProcessing.readBusData("./data/mta_1706.csv","./data/mta_1706_new.csv");
        DataPreProcessing.preprocess_cincinnati();
//        for (int i=0;i <10;++i)
//            FileWriter2.appendNewLineToFile("./data/test.txt", "hieu tran" + i);
    }
}
