import java.util.HashMap;

public class Cincinnati_map {
    public static final String mapFile = "C://Users//aries//OneDrive - The University of Texas at Dallas//Dr_Yen_Lab//transfer_learning//projects//map_data//all.osm";

    public static final String POImapFile = "C://Users//aries//OneDrive - The University of Texas at Dallas//Dr_Yen_Lab//transfer_learning//projects//map_data//POI.csv";

    public class Node{
        String id;
        HashMap<String, String> tags;

        public Node(String id) {
            this.id = id;
            this.tags = new HashMap<>();
        }
        public void addKV(String K, String V) {
            this.tags.put(K, V);
        }
    }
    public static void process() {
        String data = FileReader2.readStringFromFile(mapFile);
        String[] lines = data.split("\n");
        System.out.println(lines.length);

        int cnt = 0;
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < lines.length-1; ++i) {
            if (isOpenAndClose(lines[i])) continue;
            if (isOpenNode(lines[i])) {
                ++cnt;
                String[] tokens = lines[i].trim().split(" ");
                String lat = "";
                String lon = "";
                for (String t : tokens) {
                    if (t.contains("lat")) {
                        lat = t.substring(5, t.length()-1);
                    } else
                        if (t.contains("lon")) {
                            lon = t.substring(5, t.length()-2);
                        }
                }
                res.append(lat).append(",").append(lon).append("\n");
            }
        }
        FileWriter2.writeStringToFile(POImapFile, res.toString());
        System.out.println(cnt);
        //lat="39.1145110" lon="-84.6026050">
    }

    public static boolean isOpenAndClose(String line) {
        return isOpenNode(line) && line.contains("/>");
    }
    public static boolean isOpenNode(String line) {
        return ("<node").equals(line.trim().substring(0, 5));
    }
    public static boolean isCloseNode(String line) {
        return line.trim().contains("</node>");
    }
    public static void main(String[] args) {
        Cincinnati_map.process();
    }
}
