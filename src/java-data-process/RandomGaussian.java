import java.util.Random;

/**
 Generate pseudo-random floating point values, with an
 approximately Gaussian (normal) distribution.

 Many physical measurements have an approximately Gaussian
 distribution; this provides a way of simulating such values.
 */
public final class RandomGaussian {

    public static void main(String... aArgs){
        RandomGaussian gaussian = new RandomGaussian();
        double MEAN = 10f;
        double VARIANCE = 5f;
        for (int idx = 1; idx <= 100; ++idx){
            double x = gaussian.getGaussian(MEAN, VARIANCE);
            if (x < 3) continue;
            log("Generated : " + x);
        }
    }

    private static Random fRandom = new Random();

    public static double getGaussian(double aMean, double aVariance){
        return aMean + fRandom.nextGaussian() * aVariance;
    }

    private static void log(Object aMsg){
        System.out.println(String.valueOf(aMsg));
    }
}