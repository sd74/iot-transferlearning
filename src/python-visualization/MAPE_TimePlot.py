import matplotlib.pyplot as plt
import FileUtils as fu


df=fu.read_csv_file_wo_headers("./input/MAPE_vs_Time.csv",
                               ["x", "IoT-DL-urban", "Non-TL-urban", "IoT-DL-suburban", "Non-TL-suburban"])

y1=df["IoT-DL-urban"].tolist()
y2=df["Non-TL-urban"].tolist()
y3=df["IoT-DL-suburban"].tolist()
y4=df["Non-TL-suburban"].tolist()

x=df["x"].tolist()
xi = list(range(len(x)))

plt.subplot(1, 2, 1)
plt.ylim(0,40)
plt.plot(xi, y1,  marker='o', markersize=10, linestyle='-', color='g', label='Transfer',  linewidth=3)
plt.plot(xi, y2,  marker='s', markersize=10, linestyle='-', color='b', label='Non-Transfer',  linewidth=3)
plt.ylabel('MAPE(%)', fontsize=16)
plt.xlabel('Time\n$(a) Urban$', fontsize=16)
# plt.xlabel('Coverage\n$(c) SP_{meaning}$', fontsize=18)

plt.xticks(xi, x)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14, rotation=90)
plt.legend(loc='upper right' ,title='Approach', fontsize=16)
plt.locator_params(axis='x', nbins=12)

plt.subplot(1, 2, 2)
plt.ylim(0,40)
plt.plot(xi, y3,  marker='o', markersize=10, linestyle='-', color='g', label='Transfer',  linewidth=3)
plt.plot(xi, y4,  marker='s', markersize=10, linestyle='-', color='b', label='Non-Transfer',  linewidth=3)

plt.ylabel('MAPE(%)', fontsize=16)
plt.xlabel('Time\n$(b) Suburban$', fontsize=16)
plt.xticks(xi, x)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14, rotation=90)
plt.legend(loc='upper right' ,title='Approach', fontsize=16)
plt.locator_params(axis='x', nbins=12)

# plt.tight_layout()
plt.show()