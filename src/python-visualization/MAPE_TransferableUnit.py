import matplotlib.pyplot as plt
import FileUtils as fu

import numpy as np

df=fu.read_csv_file_wo_headers("./input/MAPE_vs_TransferableUnit.csv", ["x", "IoT-DL", "Non-TL"])

y1=df["IoT-DL"].tolist()
y2=df["Non-TL"].tolist()

x=df["x"].tolist()
# xi = list(range(len(x)))
xi = np.arange(len(x))


plt.subplot(1, 1, 1)
p2 = plt.bar(xi - 0.2, y1, 0.4, color='green',  label='Urban')
p2_2 = plt.bar(xi + 0.2, y2, 0.4, color='blue',  label='Suburban')
# plt.bar_label(p2, label_type='edge')
# plt.bar_label(p2_2, label_type='edge')
plt.ylabel('MAPE(%)', fontsize=18)
plt.xlabel('Transferable Units', fontsize=18)
plt.xticks(xi, x)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14, rotation=45)

plt.legend(loc='upper right' ,title='Dataset', fontsize=14)
plt.show()