import matplotlib.pyplot as plt
import FileUtils as fu

import numpy as np

df=fu.read_csv_file_wo_headers("./input/MAPE_vs_TrafficEvent.csv",
                               ["x", "IoT-DL-urban", "Non-TL-urban", "IoT-DL-suburban", "Non-TL-suburban"])

y1 = df["IoT-DL-urban"].tolist()
y2 = df["Non-TL-urban"].tolist()
y3 = df["IoT-DL-suburban"].tolist()
y4 = df["Non-TL-suburban"].tolist()

x=df["x"].tolist()
# xi = list(range(len(x)))
xi = np.arange(len(x))

plt.subplot(1, 2, 1)
plt.ylim(0,40)
p1 = plt.bar(xi - 0.2, y1, 0.4, color='green', label='Transfer')
p1_2 = plt.bar(xi + 0.2, y2, 0.4, color='blue', label='Non-Transfer')
# Bar_label show value of each bar, but we don't need it know
# plt.bar_label(p1, label_type='edge')
# plt.bar_label(p1_2, label_type='edge')
plt.ylabel('MAPE(%)', fontsize=18)
plt.xlabel('Traffic event\n$(a)Urban$', fontsize=18)
plt.xticks(xi, x)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14, rotation=45)
plt.legend(loc='upper left' ,title='Approach', fontsize=14)

plt.subplot(1, 2, 2)
plt.ylim(0,40)
p2 = plt.bar(xi - 0.2, y3, 0.4, color='green',  label='Transfer')
p2_2 = plt.bar(xi + 0.2, y4, 0.4, color='blue',  label='Non-Transfer')
# plt.bar_label(p2, label_type='edge')
# plt.bar_label(p2_2, label_type='edge')
plt.ylabel('MAPE(%)', fontsize=18)
plt.xlabel('Traffic event\n$(b)Suburban$', fontsize=18)
plt.xticks(xi, x)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14, rotation=45)

# ax2 = plt.subplot(111, sharex)
# plt.plot(xi, yp,  marker='o', markersize=10, linestyle='-', color='g', label='1K',  linewidth=3)
# plt.ylabel('Growth Rate (%)', fontsize=18)
# plt.legend(loc='upper right' ,title='#New requests', fontsize=14)

plt.legend(loc='upper left' ,title='Approach', fontsize=14)
plt.show()

# plt.subplot(1, 2, 1)
# plt.ylim(5,30)
# plt.plot(xi, y1,  marker='o', markersize=10, linestyle='-', color='g', label='Transfer',  linewidth=3)
# plt.plot(xi, y2,  marker='s', markersize=10, linestyle='-', color='b', label='Non-Transfer',  linewidth=3)
# plt.ylabel('MAPE(%)', fontsize=16)
# plt.xlabel('Distance(miles)', fontsize=18)
# plt.xticks(xi, x)
# plt.xticks(fontsize=14)
# plt.yticks(fontsize=14, rotation=90)
# plt.legend(loc='upper right' ,title='Approach', fontsize=14)
# # plt.locator_params(axis='x', nbins=12)
#
# plt.subplot(1, 2, 2)
# plt.ylim(5,30)
# plt.plot(xi, y1,  marker='o', markersize=10, linestyle='-', color='g', label='Transfer',  linewidth=3)
# plt.plot(xi, y2,  marker='s', markersize=10, linestyle='-', color='b', label='Non-Transfer',  linewidth=3)
#
# plt.ylabel('MAPE(%)', fontsize=16)
# plt.xlabel('Distance(miles)', fontsize=18)
# plt.xticks(xi, x)
# plt.xticks(fontsize=14)
# plt.yticks(fontsize=14, rotation=90)

# plt.legend(loc='upper right' ,title='Approach', fontsize=14)
# # plt.locator_params(axis='x', nbins=12)
#
# # plt.tight_layout()
# plt.show()