import pandas as pd

def read_csv_file(filename):
    data = pd.read_csv(filename, sep=",", header=None)
    return data

def read_csv_file_wo_headers(filename, columns_name):
    data=read_csv_file(filename)
    data.columns = columns_name
    return data