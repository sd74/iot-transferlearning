#from importlib import reload
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import MinMaxScaler
import tensorflow as tf
import folium # goelogical map


import matplotlib
matplotlib.rcParams['figure.figsize']=(10,18)
%matplotlib inline
from datetime import datetime
from datetime import date
from sklearn.cluster import MiniBatchKMeans
import seaborn as sns # plot beautiful charts
import warnings
sns.set()
warnings.filterwarnings('ignore')

#%%

from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers import Input, Dense, Activation,Dropout,LSTM
from tensorflow.keras.models import Model, Sequential
import eli5
from eli5.sklearn import PermutationImportance

import time
#from memory_profiler import profile

def build_base_model(X, z, X_trans, z_trans, epoch, layer1=512, layer2=256, layer3=128, layer4=64, layer5=16):
    model = Sequential([
        Input(shape=(X.shape[1],)),
        Dense(50, activation='relu')
    ])


# %%


def build_final_input(df,
                      heading,weather,light,road,
                      loc):
    to_traindf = df[['LONGITUDE', 'LATITUDE','LONG_DEST', 'LAT_DEST',
                    'SPEED',
                    'SPEED_AVERAGE_LAST10',
                    'DISTANCE_LAST10',
                    'rest_day','weekend','time',
                    'haversind_dist', 'manhattan_dist', 'bearing',
                    'CRASH', 'BLOCKAGE'
                 ]]
    to_traindf  = pd.concat([to_traindf,
                         heading,weather,light,road,
                         loc
                        ], axis=1)

    return to_traindf



def run_in_once(X, z, X_trans, z_trans, epoch, layer1=512, layer2=256, layer3=128, layer4=64, layer5=16):
    start = time.perf_counter()

    X_train, X_test, y_train, y_test = train_test_split(X, z, test_size=0.2, random_state=0)
    X2 = X_trans.copy()

    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)
    X_test_trans = sc.transform(X_trans)

    model = Sequential()
    model.add(Input(shape=(X.shape[1],)))
    model.add(Dropout(0.1))
    model.add(Dense(layer1, activation='relu'))
    model.add(Dropout(0.1))
    model.add(Dense(layer2, activation='relu'))
    model.add(Dropout(0.1))
    model.add(Dense(layer3, activation='relu'))
    model.add(Dropout(0.1))
    model.add(Dense(1))

    opt = tf.keras.optimizers.Adam(learning_rate=0.001)  # by default
    model.compile(optimizer=opt,
                  loss=tf.keras.losses.MeanAbsolutePercentageError(),
                  metrics=[tf.keras.metrics.MeanAbsolutePercentageError()])

    history = model.fit(X_train, y_train, epochs=epoch, verbose=1, validation_split=0.2)

    print("==>> Evaludate model itself:")
    score = model.evaluate(X_test, y_test, verbose=2)
    print("==>> Evaludate model for transfer:")
    score_trans = model.evaluate(X_test_trans, z_trans, verbose=2)

    print("Regular score (MAPE): ", score)
    print("Transfer score (MAPE): ", score_trans)

    y_predict_trans = model.predict(X_test_trans, verbose=2)
    X2['predicted'] = model.predict(X_test_trans, verbose=2)

    print(history.history.keys())
    # "Loss"
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper left')
    plt.show()

    model.summary()
    model.output_shape

    elapsed = time.perf_counter() - start
    print('Elapsed %.3f seconds.' % elapsed)
    return model, X_test, y_test, X_test_trans, z_trans, y_predict_trans, X2
