
#from importlib import reload
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import MinMaxScaler
import tensorflow as tf
import folium # goelogical map


import matplotlib
matplotlib.rcParams['figure.figsize']=(10,18)
%matplotlib inline
from datetime import datetime
from datetime import date
from sklearn.cluster import MiniBatchKMeans
import seaborn as sns # plot beautiful charts
import warnings
sns.set()
warnings.filterwarnings('ignore')


gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
  # Restrict TensorFlow to only use the first GPU
  try:
    tf.config.experimental.set_visible_devices(gpus[0], 'GPU')
    logical_gpus = tf.config.experimental.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
  except RuntimeError as e:
    # Visible devices must be set before GPUs have been initialized
    print("There is Error:")
    print(e)


def import_data():
    # Data_Dir = "C:\\Users\\aries\\Desktop\\cincinnati\\"
    Data_Dir = "C:\\Users\\aries\\OneDrive - The University of Texas at Dallas\Dr_Yen_Lab\\transfer_learning\\projects\\data\\cincinnati\\"
    # GPSData = Data_Dir + "GPS_2018_full.csv"
    DataFile1 = "GPS_2018_car_4.csv"
    DataFile2 = "GPS_2018_servicesVehicles_4.csv"
    # DataFile1 = "GPS_2018_ambulance.csv"
    GPSData = Data_Dir + DataFile1
    GPSData_trans = Data_Dir + DataFile2

    gps_train = pd.read_csv(GPSData, parse_dates=['TIME'])
    gps_train_trans = pd.read_csv(GPSData_trans, parse_dates=['TIME'])
    gps_train.info()

    print("regular-vehicle:\n", gps_train['DRIVING_TIME'].describe())
    print("\n service-vehicle:\n", gps_train_trans['DRIVING_TIME'].describe())
    gps_train.head()

    gps_train.describe()


def data_split():
    # Urban vs suburban
    # urban location
    lat_min = 39.0
    lat_max = 39.15
    long_min = -84.58
    long_max = -84.40
    long_mid = -84.53

    # condition = (lat_min < gps_train["LATITUDE"]) & (gps_train["LATITUDE"] < lat_max) & (long_min < gps_train["LONGITUDE"]) & (gps_train["LONGITUDE"] < long_max)
    condition = long_mid < gps_train["LONGITUDE"]

    gps_train_u = gps_train[condition]
    gps_train_s = gps_train[~condition]

    print(gps_train.shape)
    print("Urban size: ", gps_train_u.shape)
    print("Suburban size: ", gps_train_s.shape)

    lat_min = 39.0
    lat_max = 39.757
    long_min = -84.62
    long_max = -84.40
    # condition2 = (lat_min < gps_train_trans["LATITUDE"]) & (gps_train_trans["LATITUDE"] < lat_max) & (long_min < gps_train_trans["LONGITUDE"]) & (gps_train_trans["LONGITUDE"] < long_max)
    condition2 = long_mid < gps_train["LONGITUDE"]
    gps_train_trans_u = gps_train_trans[condition2]
    gps_train_trans_s = gps_train_trans[~condition2]

    print(gps_train_trans.shape)
    print("Urban trans size: ", gps_train_trans_u.shape)
    print("Suburban trans size: ", gps_train_trans_s.shape)


import folium # goelogical map
def show_map(df):
    map_1 = folium.Map(location=[39.3,-84.4],tiles='OpenStreetMap',zoom_start=12)
    for each in df[:1000].iterrows():
        folium.CircleMarker([each[1]['LATITUDE'],each[1]['LONGITUDE']],
                            radius=3,
                            color='red',
                            popup=str(each[1]['LAT_DEST'])+','+str(each[1]['LONG_DEST']),
                            fill_color='#FD8A6C'
                            ).add_to(map_1)
    return map_1
#show_map(gps_train_u)

def extract_time(df):
    df['year']  = df['TIME'].dt.year
    df['month'] = df['TIME'].dt.month
    df['day']   = df['TIME'].dt.day
    df['hr']    = df['TIME'].dt.hour
    df['minute']= df['TIME'].dt.minute


def time_process():
    # %%
    extract_time(gps_train_u)
    extract_time(gps_train_trans_u)

    ####
    extract_time(gps_train_s)
    extract_time(gps_train_trans_s)

    gps_train_u.head()


from datetime import datetime
from datetime import date

def restday(yr,month,day,holidays):
    '''
    Output:
        is_rest: a list of Boolean variable indicating if the sample occurred in the rest day.
        is_weekend: a list of Boolean variable indicating if the sample occurred in the weekend.
    '''
    is_rest    = [None]*len(yr)
    is_weekend = [None]*len(yr)
    i=0
    for yy,mm,dd in zip(yr,month,day):
        is_weekend[i] = date(yy,mm,dd).isoweekday() in (6,7)
        is_rest[i]    = is_weekend[i] or date(yy,mm,dd) in holidays
        i+=1
    return is_rest,is_weekend

holiday = pd.read_csv('./data/cincinnati/2018Holidays.csv',sep=';')
holiday['Date'] = holiday['Date'].apply(lambda x: x + ' 2018')
holidays = [datetime.strptime(holiday.loc[i,'Date'], '%B %d %Y').date() for i in range(len(holiday))]



#%%

###
def computeTime(df, holidays):
    rest_day,weekend = restday(df.year, df.month, df.day, holidays)
    df = df.assign(rest_day=rest_day)
    df = df.assign(weekend=weekend)
    df = df.assign(time = df.hr + df.minute/60)#float value. E.g. 7.5 means 7:30 am
    return df

gps_train_u = computeTime(gps_train_u, holidays)
# time_train_u.to_csv('./data/cincinnati/features/time_train_u.csv',index=False)

gps_train_trans_u = computeTime(gps_train_trans_u, holidays)
# # time_train_trans_u.to_csv('./data/cincinnati/features/time_train_trans_u.csv',index=False)

gps_train_s = computeTime(gps_train_s, holidays)
# # time_train_s.to_csv('./data/cincinnati/features/time_train_s.csv',index=False)

gps_train_trans_s = computeTime(gps_train_trans_s, holidays)
# # time_train_trans_s.to_csv('./data/cincinnati/features/time_train_trans_s.csv',index=False)
gps_train_trans_s.head()



def haversine_array(lat1, lng1, lat2, lng2):
    lat1, lng1, lat2, lng2 = map(np.radians, (lat1, lng1, lat2, lng2))
    AVG_EARTH_RADIUS = 6371  # in km
    lat = lat2 - lat1
    lng = lng2 - lng1
    d = np.sin(lat * 0.5) ** 2 + np.cos(lat1) * np.cos(lat2) * np.sin(lng * 0.5) ** 2
    h = 2 * AVG_EARTH_RADIUS * np.arcsin(np.sqrt(d))
    return h

def dummy_manhattan_distance(lat1, lng1, lat2, lng2):
    a = haversine_array(lat1, lng1, lat1, lng2)
    b = haversine_array(lat1, lng1, lat2, lng1)
    return a + b

def bearing_array(lat1, lng1, lat2, lng2):
    lng_delta_rad = np.radians(lng2 - lng1)
    lat1, lng1, lat2, lng2 = map(np.radians, (lat1, lng1, lat2, lng2))
    y = np.sin(lng_delta_rad) * np.cos(lat2)
    x = np.cos(lat1) * np.sin(lat2) - np.sin(lat1) * np.cos(lat2) * np.cos(lng_delta_rad)
    return np.degrees(np.arctan2(y, x))


def add_distance(df):
    lat1, lng1, lat2, lng2 = (df['LATITUDE'].values, df['LONGITUDE'].values,
                              df['LAT_DEST'].values, df['LONG_DEST'].values)

    df = df.assign(haversind_dist=haversine_array(lat1, lng1, lat2, lng2))
    df = df.assign(manhattan_dist=dummy_manhattan_distance(lat1, lng1, lat2, lng2))
    df = df.assign(bearing=bearing_array(lat1, lng1, lat2, lng2))

    return df


gps_train_u = add_distance(gps_train_u)
# Other_dist_train_u.to_csv('./data/cincinnati/features/Other_dist_train_u.csv',index=False)

gps_train_trans_u = add_distance(gps_train_trans_u)
# Other_dist_train_trans_u.to_csv('./data/cincinnati/features/Other_dist_train_trans_u.csv',index=False)

gps_train_s = add_distance(gps_train_s)
# Other_dist_train_s.to_csv('./data/cincinnati/features/Other_dist_train_s.csv',index=False)

gps_train_trans_s = add_distance(gps_train_trans_s)
# Other_dist_train_trans_s.to_csv('./data/cincinnati/features/Other_dist_train_trans_s.csv',index=False)

gps_train_trans_u.head()


def cluster_loc(df, df_trans):
    coord_source = np.vstack((df[['LATITUDE', 'LONGITUDE']].values,
                              df_trans[['LATITUDE', 'LONGITUDE']].values))
    coord_dest = np.vstack((df[['LAT_DEST', 'LONG_DEST']].values,
                            df_trans[['LAT_DEST', 'LONG_DEST']].values))

    coords = np.hstack((coord_source, coord_dest))  # 4 dimensional data
    sample_ind = np.random.permutation(len(coords))[:500000]
    kmeans = MiniBatchKMeans(n_clusters=10, batch_size=10000).fit(coords[sample_ind])

    def add_cluster_column(dff):
        dff.loc[:, 'source_dest_loc'] = kmeans.predict(dff[['LATITUDE', 'LONGITUDE', 'LAT_DEST', 'LONG_DEST']])

    add_cluster_column(df)
    add_cluster_column(df_trans)

    return df, df_trans


gps_train_u, gps_train_trans_u = cluster_loc(gps_train_u, gps_train_trans_u)
# gps_train_u.to_csv('./data/cincinnati/features/kmean10_train_u.csv',index=False,columns = ['source_dest_loc'])
# gps_train_trans_u.to_csv('./data/cincinnati/features/kmean10_train_trans_u.csv',index=False,columns = ['source_dest_loc'])

gps_train_s, gps_train_trans_s = cluster_loc(gps_train_s, gps_train_trans_s)
# gps_train_s.to_csv('./data/cincinnati/features/kmean10_train_s.csv',index=False,columns = ['source_dest_loc'])
# gps_train_trans_s.to_csv('./data/cincinnati/features/kmean10_train_trans_s.csv',index=False,columns = ['source_dest_loc'])


# plot clustring for train
def draw_cluser(df):
    plt.figure(figsize=(16,16))
    N = 500
    for i in range(10):
        plt.subplot(4,3,i+1)
        tmp_data = df[df.source_dest_loc==i]
        source = plt.scatter(tmp_data['LATITUDE'][:N], tmp_data['LONGITUDE'][:N], s=10, lw=0, alpha=0.5,label='SOURCE')
        dest = plt.scatter(tmp_data['LAT_DEST'][:N], tmp_data['LONG_DEST'][:N], s=10, lw=0, alpha=0.4,label='DEST')
        plt.xlim([38,40]);plt.ylim([-85,-83])
        plt.legend(handles = [source,dest])
        plt.title('clusters %d'%i)
#plt.axes().set_aspect('equal')

# plot clustring for train
def draw_cluser(df):
    plt.figure(figsize=(16,16))
    N = 500
    for i in range(10):
        plt.subplot(4,3,i+1)
        tmp_data = df[df.source_dest_loc==i]
        source = plt.scatter(tmp_data['LATITUDE'][:N], tmp_data['LONGITUDE'][:N], s=10, lw=0, alpha=0.5,label='SOURCE')
        dest = plt.scatter(tmp_data['LAT_DEST'][:N], tmp_data['LONG_DEST'][:N], s=10, lw=0, alpha=0.4,label='DEST')
        plt.xlim([38,40]);plt.ylim([-85,-83])
        plt.legend(handles = [source,dest])
        plt.title('clusters %d'%i)
#plt.axes().set_aspect('equal')


model_u, X_test_u1, y_test_u1, X_test_trans_u1, z_trans_u1, y_predict_trans_u, X2_u = run_in_once(
                                                                    X_u, z_u, X_trans_u, z_trans_u, epoch=30,
                                                                    layer1=1000, layer2=500, layer3=100)

def construct_transfer():
    model_u.trainable=False
    input2=Input(shape=(X_u.shape[1],))
    x = model_u(input2, training=False)
    output2 = tf.keras.layers.Dense(1)(x)
    model_new = tf.keras.Model(input2, output2)
    model_new.summary()
    opt = tf.keras.optimizers.Adam(learning_rate=0.001)  # by default
    model_new.compile(optimizer=opt,
                      loss=tf.keras.losses.MeanAbsolutePercentageError(),
                      metrics=[tf.keras.metrics.MeanAbsolutePercentageError()])
    history_new = model_new.fit(X_test_trans_u1, z_trans_u1, epochs=20, verbose=1, validation_split=0.2)

    print(history_new.history.keys())
    # "Loss"
    plt.plot(history_new.history['loss'])
    plt.plot(history_new.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper left')
    plt.show()


def prediction():
    model_s, X_test_s1, y_test_s1, X_test_trans_s1, z_trans_s1, y_predict_trans_s, X2_s = run_in_once(
        X_s, z_s, X_trans_s, z_trans_s, epoch=30,
        layer1=1000, layer2=500, layer3=100)

    print(y_predict_trans_s.shape)
    print(X_trans_s.shape)
    print(z_trans_s.shape)
    X2_s.head()

def TL2():
    model_s.trainable = False
    input3 = Input(shape=(X_s.shape[1],))
    x = model_s(input3, training=False)
    x2 = tf.keras.layers.Dense(10)(x)
    output3 = tf.keras.layers.Dense(1)(x2)
    model_new_s = tf.keras.Model(input3, output3)
    model_new_s.summary()

    opt = tf.keras.optimizers.Adam(learning_rate=0.001)  # by default
    model_new_s.compile(optimizer=opt,
                        loss=tf.keras.losses.MeanAbsolutePercentageError(),
                        metrics=[tf.keras.metrics.MeanAbsolutePercentageError()])
    history_new_s = model_new_s.fit(X_test_trans_s1, z_trans_s1, epochs=100, verbose=1, validation_split=0.2)

    print(history_new_s.history.keys())
    # "Loss"
    plt.plot(history_new_s.history['loss'])
    plt.plot(history_new_s.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper left')
    plt.show()

    model_new_s.summary()

% % time
sc_mapping = StandardScaler()


def NN_mapping(X2, z2, layer1=1000, layer2=500, layer3=100, epoch=5):
    X2_train, X2_test, y2_train, y2_test = train_test_split(X2, z2, test_size=0.2, random_state=0)

    X2_train = sc_mapping.fit_transform(X2_train)
    X2_test = sc_mapping.transform(X2_test)

    model2 = Sequential()
    model2.add(Input(shape=(X2.shape[1],)))
    model2.add(Dropout(0.2))
    model2.add(Dense(layer1, activation='relu'))
    model2.add(Dropout(0.2))
    model2.add(Dense(layer2, activation='relu'))
    model2.add(Dropout(0.2))
    model2.add(Dense(layer3, activation='relu'))
    model2.add(Dropout(0.2))
    model2.add(Dense(1))

    #     model.compile(loss='mean_squared_error', optimizer='adam' ,metrics=[tf.keras.metrics.RootMeanSquaredError()])
    opt = tf.keras.optimizers.Adam(learning_rate=0.001)  # by default
    model2.compile(optimizer=opt,
                   loss=tf.keras.losses.MeanAbsolutePercentageError(),
                   metrics=[tf.keras.metrics.MeanAbsolutePercentageError()])

    history = model2.fit(X2_train, y2_train, epochs=epoch, verbose=1, validation_split=0.2)

    score = model2.evaluate(X2_test, y2_test, verbose=2)
    print("Score mapping (MAPE): ", score)

    print(history.history.keys())
    # "Loss"
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper left')
    plt.show()

    return model2, X2_train, X2_test, y2_train, y2_test
%%time
model_mapping, X2_train_u, X2_test_u, y2_train_u, y2_test_u = NN_mapping(X2_u, z_trans_u, layer1=1000, layer2=500, layer3=100, epoch=30)

%%time
def apply_mapping(X, z, model_mapping):
    X_test = sc_mapping.transform(X)
    score = model_mapping.evaluate(X_test,  z, verbose=2)
    print("Score mapping (MAPE): ", score)
    return X_test, score
%%time
X2_s1, score1 = apply_mapping(X2_s, z_trans_s, model_mapping)



%%time
# this for feature important analysis
# References: https://www.kaggle.com/ourique/permutation-importance
# https://scikit-learn.org/stable/modules/permutation_importance.html
# Analysis with same data
perm = PermutationImportance(model_u, random_state=1,scoring="neg_root_mean_squared_error").fit(X_test_u1, y_test_u1)
eli5.show_weights(perm, feature_names = X_u.columns.tolist())


%%time
# Analysis with different data
perm = PermutationImportance(model_u, random_state=1,scoring="neg_root_mean_squared_error").fit(X_test_trans_u1[:10000], z_trans_u1[:10000])
eli5.show_weights(perm, feature_names = X_u.columns.tolist())


%%time
# this for feature important analysis
# References: https://www.kaggle.com/ourique/permutation-importance
perm = PermutationImportance(model_mapping, random_state=1,scoring="neg_root_mean_squared_error").fit(X2_s1[:10000], z_trans_s[:10000])
eli5.show_weights(perm, feature_names = X2_s.columns.tolist())













